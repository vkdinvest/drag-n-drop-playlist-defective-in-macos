# drag n drop playlist defective in macOS

drag-drop-playlist-defective-in-macos

WTF?

Up to latest version, 3.0.16, of VLC media player, dragging and dropping a folder of audio files to the app icon in macOS would load said files into the playlist in the correct (numbered) order, including any subfolders in subsequent sequence. What now happens is, the main folder of files is added and the subfolder content remains invisible in the playlist, until the play order reaches the end of the mother folder's content, ie. until the first file in teh first subfolder is played. At that point, teh content of that subfolder is shown in the VLC Playlist. This happens with each subsequent subfolder in turn. 

However, I have just encountered a different behaviour, where the subfolder content was listed first and played first, then the main folder's content was played but never became visible in the Playlist; the name of the audio file being played, though, was visible in the app Title Bar. 

Cannot see any way to fix this. Any clues?

Please add replies to me at vkd108@hotmail.com as I have a problem with this new 2 factor authentication, thanks.
